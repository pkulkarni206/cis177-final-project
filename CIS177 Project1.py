'''
Pranav Kulkarni
CIS 177 Project
'''

from tkinter import *
import random


SIZE_OF_HAND = 2

class DeckofCardsGUI:
    def  __init__(self):
        window = Tk()
        window.title("First Player to get a 5 of Clubs Wins!")

        self.imageList = []
        for i in range (1,53):
            self.imageList.append(PhotoImage(file = "cards/" + str(i) + ".gif"))

        frame = Frame(window)
        frame.pack()

        self.labelList = []
        for i in range(SIZE_OF_HAND):
            self.labelList.append(Label(frame, image = self.imageList[i]))
            self.labelList[i].pack(side = LEFT)

        Button(window, text="Try Again!", command=self.shuffle).pack()
        Button(window, text="Player One Wins!",).pack()
        Button(window, text="Player Two Wins!").pack()

        window.mainloop()

    def shuffle(self):
        random.shuffle(self.imageList)
        for i in range(SIZE_OF_HAND):
            self.labelList[i]["image"] = self.imageList[i]




DeckofCardsGUI()
